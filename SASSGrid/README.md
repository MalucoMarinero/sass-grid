# SASS Grid

SASS Grid is a small framework for making a flexible grid in SASS. You choose the grid, you choose how to apply it, and you don't have to touch your HTML.

## Preparing SASS

SASS unfortunately does not calculate divisions to high degree of precision by default. This can make layouts that have tight margins that use up 100% of space problematic. Luckily, changing this is not difficult.

If you use SASS directly, you can use the option **--precision** to set the precision to 10 or something like that.

If you use Compass, you'll need to adjust a file inside SASS to change defaults. Inside the SASS Gem, go to: lib/sass/script/number.rb

Go to line 39 or there abouts. You should see a number that represents the precision SASS renders to. Simply change this to 10 and from now on your percentages will be much more precise.

## Installation & Setup

Just copy this into your SASS or Compass Project for now and @import it as you would any other SASS or SCSS file. While it does come with a default grid, you can set your own up by setting the following variables:

    $grid0_unit_count: 12
Sets the number of units that makes up your grid.

    $grid0_left_margin_ratio: 1
The size of the left most margin in comparison to the rest of the grid.

    $grid0_right_margin_ratio: 1
The size of the right most margin in comparison to the rest of the grid.

    $grid0_unit_ratio: 6
The size of the units in comparison to the rest of the grid. The units are essentially the part your content would be in.

    $grid0_gutter_ratio: 2
The size of the gutters in comparison to the rest of the grid. That is the space in between each unit.

You need to put these variables in BEFORE importing the `_grid.sass` file.

### Why grid0? 

In the future I'll probably copy paste in one or two more mixin sets which use variables grid1 and grid2, that way you can use wore than one type of grid depending on your needs.

## Usage

Two mixins are created which allow you to apply your grid to an element.

    +grid0_region
    +grid0_column

Regions only go inside something that is using the full width of the page. These are the top level of the grid as it were.
    section.focus
      +grid0_region(8, 0, 1, ('first_margin_left', 'margin_right'))
      float: left
    section.aside
      +grid0_region(3, 0, 0, ('no_margin_left', 'first_margin_right'))
      float: left

This creates two regions, the first one:
* Is 8 units wide
* Has a margin that takes up 0 units on its left. Because it's the `first_margin_left`, it uses the left margin ratio, rather than the gutter.
* Has a margin that takes up 1 unit on its right.

The second region is:
* 3 units wide.
* Has no margin on it's left. We already provided the gap between the two regions in the first one.
* Has a margin that takes up 0 units on its right. Because it's the `first_margin_right`, it uses the right margin ratio, rather than the gutter.


Columns go inside regions. They need to be told how big the region it is inside is.
    section.focus
      +grid0_region(8, 0, 1, ('first_margin_left', 'margin_right'))
      float: left
      section.image
        +grid0_column(3, 8, 1, 0, ('margin_left', 'no_margin_right'))
	float: right

The column, `section.image`, will go inside `section.focus` and be:
* 3 units wide inside an 8 unit container
* Has a margin on the left of 1 unit. It's just `margin_left` so it uses the gutter ratio.
* Has no margin on it's right. This way it'll be snug up against the edge of `section.focus`.

## Early days yet
Have a play, just thought I'd put this out there if anyones interested in using a sort of grid framework with out the classes littered throughout your HTML.






	
